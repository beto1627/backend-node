const express = require('express');
const body_parser = require('body-parser');
require('dotenv').config();//no almacenamos en una constante
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v1/';
const usersFile = require('./user.json')

app.listen(port, function(){
 console.log('Node JS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

//Operación GET (Collection)
app.get(URL_BASE + 'users',
   function(request, response){
   response.status(201);
   response.send(usersFile);
});

//Petición GET a un único usuarios mediante ID (Instancia)
app.get(URL_BASE + 'users/:id',
   function(request, response){
   console.log(request.params.id);
   let pos = request.params.id - 1;
   let user = usersFile[pos];
   let respuesta = (user == undefined) ? {'mensaje' : 'usuario no existente'} : user;
   let status = (user == undefined) ? 204 : 200;
   response.status(status);
   response.send(user);
});

//Operación GET con QUERY STRING
app.get(URL_BASE + 'usersq',
   function(request, response){
   console.log(request.query.id);
   console.log(request.query.country);
   response.send({'id2:':request.query.id, 'country2: ':request.query.country});
});

//Petición POST a users
app.post(URL_BASE + 'users',
   function(request, response){
   console.log('POST a users');
   let tam = usersFile.length;
   let new_user = {
     "id_user":tam + 1,
     "first_name":request.body.first_name,
     "last_name":request.body.last_name,
     "email":request.body.email,
     "password":request.body.password
   }
   console.log(new_user);
   usersFile.push(new_user);
   response.status(201);
   response.send({"msg":"Usuario creado correctamente"});
});

//Petición PUT a users
app.put(URL_BASE + 'users/:id',
   function(request, response){
   console.log('PUT a users');
   let upd_user = usersFile[request.params.id-1];
   let status;
   let respuesta;
   if(upd_user == undefined){
     respuesta = {'mensaje' : 'usuario no existente'};
     status = 404;
   }else{
     upd_user.first_name = request.body.first_name;
     upd_user.last_name = request.body.last_name;
     upd_user.email = request.body.email;
     upd_user.password = request.body.password;
     console.log(upd_user);
     respuesta = {"msg":"Usuario actualizado correctamente"};
     status = 200;
   }
   response.status(status);
   response.send(respuesta);
});


//Petición DELETE a users
app.delete(URL_BASE + 'users/:id',
   function(request, response){
   console.log('DELETE a users');
   usersFile.splice(request.params.id-1,1);
   response.status(200);
   response.send({"msg":"Usuario eliminado correctamente"});
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'user.json'.");
     }
   });
}
// LOGIN - user.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.send({"msg" : "Login correcto.", "idUsuario" : us.id_user});
        } else {
          console.log("Login incorrecto.");
          response.send({"msg" : "Login incorrecto."});
        }
      }
    }
    if(us.logged != true){
      console.log("Login incorrecto.");
      response.send({"msg" : "Login incorrecto."});
    }
});

// LOGOUT - user.json
app.post(URL_BASE + 'logout',
  function(request, response) {
    console.log("POST /apicol/v2/logout");
    var userId = request.body.id;
    var valida = false;
    for(us of usersFile) {
      if(us.id_user == userId) {
        if(us.logged) {
          valida = true;
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersFile);
          console.log("Logout correcto!");
          response.send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
        } else {
          console.log("Logout incorrecto.");
          response.send({"msg" : "Logout incorrecto."});
        }
      }
    }
    if(!valida){
      console.log("Logout incorrecto.");
      response.send({"msg" : "Logout incorrecto."});
    }
});

//Operación GET total
app.get(URL_BASE + 'total_users',
   function(request, response){
   response.status(200);
   response.send({"num_usuarios" : usersFile.length});
});
